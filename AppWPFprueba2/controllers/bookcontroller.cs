﻿using AppWPFprueba2.model;
using AppWPFprueba2.views;
using Microsoft.Win32;
using System;
using System.Windows;
using System.Windows.Controls;

namespace AppWPFprueba2.controllers
{
    class bookcontroller
    {
        object Lwindow;
        SaveFileDialog sfdialog;
        OpenFileDialog ofdialog;
        public bookcontroller(LibroWindow window)
        {
            Lwindow = window;
            sfdialog = new SaveFileDialog();
            ofdialog = new OpenFileDialog();
        }

        public void bookEventHandler(object sender, RoutedEventArgs e)
        {
            Button B = (Button)sender;
            switch (B.Name)
            {
                case "SaveButton":
                    SaveDate();
                    break;
                case "OpenButton":
                    OpenFile();
                    break;
            }
        }

        public void OpenFile()
        {
            ofdialog.Filter = "Json File (*.json)|*.json";
            if (ofdialog.ShowDialog() == true)
            {
                libro l = new libro();
                if (Lwindow.GetType().Equals(typeof(LibroWindow)))
                {
                    ((LibroWindow)Lwindow).SetData(l.FromJson(ofdialog.FileName));
                }

            }
        }
        public void SaveDate()
        {
            sfdialog.Filter = "Json File (*.json)|*.json";
            if (sfdialog.ShowDialog() == true)
            {
                libro l;
                if (Lwindow.GetType().Equals(typeof(LibroWindow)))
                {
                    l= ((LibroWindow)Lwindow).GetData();
                }
                

                l.ToJson(sfdialog.FileName);
            }

        }
    }
    }

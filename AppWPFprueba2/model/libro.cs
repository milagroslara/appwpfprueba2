﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AppWPFprueba2.Serializacion.JsonSerializacion;

namespace AppWPFprueba2.model
{
    class libro
    {
        public string titulo { get; set; }
        public string nombre_del_autor { get; set; }
        public string editorial { get; set; }
        public string categorias { get; set; }

        public void ToJson(string filepath)
        {
            JsonSerialization.WriteToJsonFile(filepath, this);
        }

        internal libro FromJson(string fileName)
        {
            throw new NotImplementedException();
        }
    }
}

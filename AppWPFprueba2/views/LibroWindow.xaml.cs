﻿using AppWPFprueba2.controllers;
using AppWPFprueba2.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AppWPFprueba2.views
{
    /// <summary>
    /// Interaction logic for LibroWindow.xaml
    /// </summary>
    public partial class LibroWindow : Window
    {
        bookcontroller bc;

        public LibroWindow()
        {
            InitializeComponent();
            SetupController();
        }

        private void SetupController()
        {
            bc = new bookcontroller(this);
            this.SaveButton.Click += new RoutedEventHandler(bc.bookEventHandler);
            this.OpenButton.Click += new RoutedEventHandler(bc.bookEventHandler);
        }

        internal void SetData(libro book)
        {
            NameBook.Text = book.titulo;
            nameautor.Text = book.nombre_del_autor;
            datePiker.SelectedDate = book.editorial;
            CategoryCombobox.SelectedValue = book.categorias;
        }

        internal libro GetData()
        {
            libro book = new libro();
            book.titulo = NameBook.Text;
            book.nombre_del_autor = nameautor.Text;
            book.editorial = (DatePicker)datePiker.
            book.categorias = CategoryCombobox.SelectedValue.ToString();

            return book;

        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppWPFprueba2.JFrom
{
    public interface Jform<T>
    { 
    T FromJson(string filepath);
    
    }
}
